package com.alric.iem.appbourgtub;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by iem on 03/02/2017.
 */

public class ClusteredItem implements ClusterItem{
    private final LatLng mPosition;
    private final String title;


    public ClusteredItem(double lat, double lng,String title) {
        mPosition = new LatLng(lat, lng);
        this.title=title;
    }
    public String getTitle(){
        return title;
    }
    @Override
    public LatLng getPosition() {
        return mPosition;
    }
}

