package com.alric.iem.appbourgtub;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.ListView;
import android.widget.RelativeLayout;

/**
 * Created by iem on 28/02/2017.
 */

public class CustomDrawerLayout extends DrawerLayout {
    private Animator mAnimator;


    public CustomDrawerLayout(Context context) {
        super(context);

    }

    public CustomDrawerLayout(Context context, AttributeSet attrs) {
        super(context, attrs);

    }

    public CustomDrawerLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

    }

    public void openDrawer(View drawerView, RelativeLayout mLanguette, ListView mDrawerList) {

        super.openDrawer(GravityCompat.START);

    }
}

