package com.alric.iem.appbourgtub;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

/**
 * Created by iem on 03/02/2017.
 */

public class OwnIconRendered extends DefaultClusterRenderer<ClusteredItem> {
    public OwnIconRendered(Context context, GoogleMap map,
                           ClusterManager<ClusteredItem> clusterManager) {
        super(context, map, clusterManager);
    }

    @Override
    protected void onBeforeClusterItemRendered(ClusteredItem item, MarkerOptions markerOptions) {
        markerOptions.title(item.getTitle());
        super.onBeforeClusterItemRendered(item, markerOptions);
    }

}
