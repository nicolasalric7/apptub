package com.alric.iem.appbourgtub;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by iem on 02/11/2016.
 */

public class SqliteHelper extends SQLiteOpenHelper {
    
    private static final String DATABASE_NAME = "tub.db";
    private static final int DATABASE_VERSION = 1;

    private static final String LIGNE_ID = "idligne";
    private static final String STOP_ID = "idstop";
    private static final String HORAIRE_ID = "idhoraire";

    private static final String LIGNE_TABLE_NAME = "Ligne";
    private static final String STOP_TABLE_NAME = "Stop";
    private static final String HORAIRE_TABLE_NAME = "Horaire";

    private static final String LIGNE_NOM = "name";
    private static final String LIGNE_DIRECTION = "direction";
    private static final String LIGNE_LISTEARRETS = "stop_list";

    private static final String HORAIRE_LISTHORAIREALLER = "horaire_aller";
    private static final String HORAIRE_LISTHORAIRERETOUR = "horaire_retour";
    private static final String HORAIRE_IDLIGNE = "ligneid";

    private static final String STOP_NOM = "name";
    private static final String STOP_LAT = "latitude";
    private static final String STOP_LONG = "longitude";

    private static final String LIGNE_TABLE_CREATE =
            "CREATE TABLE " + LIGNE_TABLE_NAME + " (" +
                    LIGNE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    LIGNE_DIRECTION + " TEXT, " +
                    LIGNE_LISTEARRETS + " TEXT, " +
                    LIGNE_NOM + " TEXT);";

    private static final String STOP_TABLE_CREATE =
            "CREATE TABLE " + STOP_TABLE_NAME + " (" +
                    STOP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    STOP_NOM + " TEXT, " +
                    STOP_LAT + " TEXT, " +
                    STOP_LONG + " TEXT);";

    private static final String HORAIRE_TABLE_CREATE =
            "CREATE TABLE " + HORAIRE_TABLE_NAME + " (" +
                    HORAIRE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    HORAIRE_LISTHORAIREALLER + " TEXT, " +
                    HORAIRE_LISTHORAIRERETOUR + " TEXT, " +
                    HORAIRE_IDLIGNE + " INTEGER, " +
                    "FOREIGN KEY " + "(" + HORAIRE_IDLIGNE + ")" + " REFERENCES " + LIGNE_TABLE_NAME + "(" + LIGNE_ID + "));";

    private Activity activity;

    public SqliteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(LIGNE_TABLE_CREATE);
        db.execSQL(STOP_TABLE_CREATE);
        db.execSQL(HORAIRE_TABLE_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
