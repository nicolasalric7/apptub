package com.alric.iem.appbourgtub.api.Service;

import android.app.Application;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by nicolaschurlet on 28/02/2017.
 */

public class InitService extends Application {
    private static Retrofit retrofit;

    public Retrofit getinstance(){
        if (retrofit == null)
        {
            retrofit = new Retrofit.Builder()
                    .baseUrl("http://nalric.me/web/index.php/api")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
    InitService service = retrofit.create(InitService.class);
    @Override
    public void onCreate() {
        super.onCreate();
    }
}
