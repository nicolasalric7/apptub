package com.alric.iem.appbourgtub.api.model;

/**
 * Created by iem on 07/03/2017.
 */

public class ArrivalTime {
    public ArrivalTime(String time) {
        this.time = time;
    }

    private String time;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
