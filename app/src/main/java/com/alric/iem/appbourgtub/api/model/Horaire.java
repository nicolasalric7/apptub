package com.alric.iem.appbourgtub.api.model;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by iem on 02/11/2016.
 */

public class Horaire implements Serializable {
    //private int id;
    private String[] listeHoraires;




    private int order;

    private String nom;

    public Horaire(String[] listeHoraires, String nom, int order) {
        this.listeHoraires = listeHoraires;
        this.nom = nom;
        this.order = order;
    }



    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    public String[] getListeHoraires() {
        return listeHoraires;
    }

    public void setListeHoraires(String[] listeHoraires) {
        this.listeHoraires = listeHoraires;
    }
    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < getListeHoraires().length; i++){
            if(getListeHoraires()[i] == null || getListeHoraires()[i].equals("....")) {
                sb.append("--:--");
                sb.append("  ");

            }
            else{
                sb.append(getListeHoraires()[i]);
                sb.append("  ");

            }

        }
        return (sb.toString());
    }
}