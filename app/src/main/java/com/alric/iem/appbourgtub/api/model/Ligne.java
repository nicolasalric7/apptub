package com.alric.iem.appbourgtub.api.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by iem on 02/11/2016.
 */

public class Ligne
{
    @SerializedName("id_line")
    private int id;
    @SerializedName("number")
    private int number;
    @SerializedName("direction")
    private String direction;

    public Ligne(int id, int number, String direction) {
        this.id = id;
        this.number = number;
        this.direction = direction;
    }

    @Override
    public String toString() {
        return "Ligne "+ number + " - " +  direction;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(String nom) {
        this.number = number;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }


}
