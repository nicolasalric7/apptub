package com.alric.iem.appbourgtub.api.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by iem on 07/03/2017.
 */

public class StopLine {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    @SerializedName("order")
    int order;
    @SerializedName("time")
    String time;
    @SerializedName("name")
    String name;
}
