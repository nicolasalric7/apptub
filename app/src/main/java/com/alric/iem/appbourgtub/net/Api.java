package com.alric.iem.appbourgtub.net;

import com.alric.iem.appbourgtub.api.model.ArrivalTime;
import com.alric.iem.appbourgtub.api.model.Ligne;
import com.alric.iem.appbourgtub.api.model.Stop;
import com.alric.iem.appbourgtub.api.model.StopLine;
import com.google.android.gms.vision.text.Line;
import com.google.gson.JsonArray;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by nicolaschurlet on 03/03/2017.
 */

public class Api {
    private ApiService apiservice;

    public Api() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://nalric.me/web/index.php/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.apiservice = retrofit.create(ApiService.class);
    }

    public Call<List<Stop>> getStopList(){
        return apiservice.stops();
    }

    public Call<List<Stop>> getStopsByLineId(String line_id){
        return apiservice.stopsByLineId(line_id);
    }
    public Call<List<ArrivalTime>> getArrivingTime(String line_id, String stop_name, String time, String destination, String way){
        return apiservice.getArrivingTime(line_id, stop_name, time, way, destination);
    }
    public Call<List<Ligne>> getAllLines(){
        return apiservice.getAllLine();
    }
    public Call<List<StopLine>> getScheduleByLine(String line_id){
        return apiservice.getScheduleByLine(line_id);
    }


    // Interface
    public interface ApiService {

        @GET("stops/list")
        Call<List<Stop>> stops();

        @GET("lines/get")
        Call<List<Ligne>> getAllLine();

        @FormUrlEncoded
        @POST("stops/getbyline")
        Call<List<Stop>> stopsByLineId(
                @Field("line_id") String id
        );

        @FormUrlEncoded
        @POST("stops/getbyline")
        Call<List<Stop>> getAllStopsFromLine(
                @Field("line_id") String id
        );

        @FormUrlEncoded
        @POST("schedule/getbyline")
        Call<List<StopLine>> getScheduleByLine(
                @Field("line_id") String id
        );

        @FormUrlEncoded
        @POST("bus/get")
        Call<List<ArrivalTime>> getArrivingTime(
                @Field("line_id") String idLine,
                @Field("stop_name") String DepartStopName,
                @Field("time") String timeDepart,
                @Field("way") String way,
                @Field("destination_name") String ArrivalStopName
        );
    }

}
