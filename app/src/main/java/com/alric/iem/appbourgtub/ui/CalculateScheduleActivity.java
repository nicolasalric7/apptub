package com.alric.iem.appbourgtub.ui;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.alric.iem.appbourgtub.R;
import com.alric.iem.appbourgtub.api.model.ArrivalTime;
import com.alric.iem.appbourgtub.api.model.Ligne;
import com.alric.iem.appbourgtub.api.model.Stop;
import com.alric.iem.appbourgtub.net.Api;
import com.google.android.gms.vision.text.Line;
import com.google.gson.JsonArray;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.attr.lines;

/**
 * Created by iem on 03/02/2017.
 */

public class CalculateScheduleActivity extends AppCompatActivity {
    Api api = new Api();

    List<Ligne> lines;

    Spinner start;
    Spinner endPoint;
    EditText startSchedule;
    Spinner line ;
    TextView endSchedule;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent i = getIntent();
        setContentView(R.layout.activity_calculate_schedule);
        start = (Spinner) findViewById(R.id.start_edit);
        endPoint = (Spinner) findViewById(R.id.end_edit);
        startSchedule = (EditText) findViewById(R.id.heure_depart_edit);
        line = (Spinner) findViewById(R.id.line);
        endSchedule = (TextView) findViewById(R.id.heure_arrivee_to_edit);



         startSchedule.setOnClickListener(new EditText.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CalculateScheduleActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String selectedMinuteStr = String.valueOf(selectedMinute);
                        if(selectedMinuteStr.length() == 1){
                            selectedMinuteStr = "0" + selectedMinuteStr;
                        }
                        startSchedule.setText( selectedHour + ":" + selectedMinuteStr);
                    }
                }, hour, minute, true);
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();

            }
        });


        line.setOnItemSelectedListener(new Spinner.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                askForStops(position + 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });


        final Call<List<Ligne>> call = api.getAllLines();

        call.enqueue(new Callback<List<Ligne>>() {
            @Override
            public void onResponse(Call<List<Ligne>> call, Response<List<Ligne>> response) {
                lines = response.body();
                ArrayAdapter<Ligne> adapter = new ArrayAdapter<>(CalculateScheduleActivity.this, android.R.layout.simple_list_item_1, response.body());
                line.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Ligne>> call, Throwable t) {
            }
        });
    }


    private void askForStops(int i) {
        final Call<List<Stop>> call = api.getStopsByLineId(String.valueOf(i));
        Log.i("DEV2", i + " - " + lines.get(i).getNumber());
        call.enqueue(new Callback<List<Stop>>() {
            @Override
            public void onResponse(Call<List<Stop>> call, Response<List<Stop>> response) {
                ArrayAdapter<Stop> adapter = new ArrayAdapter<>(CalculateScheduleActivity.this, android.R.layout.simple_list_item_1, response.body());
                start.setAdapter(adapter);
                endPoint.setAdapter(adapter);

                start.setEnabled(true);
                start.setClickable(true);
                endPoint.setEnabled(true);
                endPoint.setClickable(true);


            }

            @Override
            public void onFailure(Call<List<Stop>> call, Throwable t) {
                Toast.makeText(CalculateScheduleActivity.this, "No Stop on line", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void calculateSchedule(View view){
        final RelativeLayout rl = (RelativeLayout) findViewById(R.id.activity_calculate_schedule);


        Button submit = (Button) findViewById(R.id.submit);
        //Overrides
        final String depart = start.getSelectedItem().toString();
        final String arrivee = endPoint.getSelectedItem().toString();
        String way = "I";
        final String time = startSchedule.getText().toString();
        final String line_id = String.valueOf(line.getSelectedItemPosition() + 1);
        if (start.getSelectedItemPosition() > endPoint.getSelectedItemPosition()) {
            way = "O";
        }
        final Call<List<ArrivalTime>> callTime = api.getArrivingTime(line_id, depart, time, arrivee, way);

        callTime.enqueue(new Callback<List<ArrivalTime>>() {
            @Override
            public void onResponse(Call<List<ArrivalTime>> call, Response<List<ArrivalTime>> response) {
                List<ArrivalTime> array = response.body();
                if (array.size() > 0) {
                    ArrivalTime answer = array.get(0);
                    Log.i("DEV2", answer.getTime());
                    if (!answer.getTime().isEmpty()) {
                        endSchedule.setText(answer.getTime());
                    }
                } else {
                    Toast.makeText(CalculateScheduleActivity.this, "No Result", Toast.LENGTH_SHORT).show();
                    System.out.println("No result");
                }
            }

            @Override
            public void onFailure(Call<List<ArrivalTime>> call, Throwable t) {
                Toast.makeText(CalculateScheduleActivity.this, "No Result", Toast.LENGTH_SHORT).show();
                t.printStackTrace();

            }
        });
    }
}