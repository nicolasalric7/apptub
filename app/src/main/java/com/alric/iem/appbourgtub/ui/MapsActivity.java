package com.alric.iem.appbourgtub.ui;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alric.iem.appbourgtub.ClusteredItem;
import com.alric.iem.appbourgtub.OwnIconRendered;
import com.alric.iem.appbourgtub.R;
import com.alric.iem.appbourgtub.api.Service.InitService;
import com.alric.iem.appbourgtub.api.model.Horaire;
import com.alric.iem.appbourgtub.api.model.Stop;
import com.alric.iem.appbourgtub.net.Api;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.gson.Gson;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.kml.KmlLayer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import au.com.bytecode.opencsv.CSVReader;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private ArrayList<Horaire> listHoraires = new ArrayList<>();
    private ArrayList<Stop> listStop = new ArrayList<>();
    private ClusterManager mClusterManager;
    private DrawerLayout mDrawerLayout;
    private RelativeLayout mLanguette;
    private LinearLayout mDrawerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mMap);
        mapFragment.getMapAsync(this);



        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (LinearLayout) findViewById(R.id.navlist);
        mLanguette = (RelativeLayout) findViewById(R.id.languette);
        initDrawer();
        //getCsvValues();
        getStops();
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {

        //region Init-Map
        mMap = googleMap;
        LatLng latLng = new LatLng(46.204440, 5.226050);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(13));
        KmlLayer layer = null;
        mMap.setMapType(GoogleMap.MAP_TYPE_TERRAIN);
        //endregion

        try {
            //region Path
            for(int i=3; i < 8; i++) {
                int drawableResourceId = this.getResources().getIdentifier("ligne0"
                        + Integer.toString(i), "raw", this.getPackageName());
                KmlLayer monLayer = new KmlLayer(mMap, drawableResourceId, getApplicationContext());
                monLayer.addLayerToMap();
            }
            layer = new KmlLayer(mMap, R.raw.ligne01, getApplicationContext());
            layer.addLayerToMap();
            layer = new KmlLayer(mMap, R.raw.ligne21, getApplicationContext());
            layer.addLayerToMap();
            layer = new KmlLayer(mMap, R.raw.ligne02ainterexpo, getApplicationContext());
            layer.addLayerToMap();
            layer = new KmlLayer(mMap, R.raw.ligne02norelan, getApplicationContext());
            layer.addLayerToMap();

            //region Callback - Retrofit2 - Cluster
            // New Api web service
            Api api = new Api();
            try {
                Call<List<Stop>> call = api.getStopList();
                call.enqueue(new Callback<List<Stop>>() {
                    @Override
                    public void onResponse(Call<List<Stop>> call, Response<List<Stop>> response) {
                        List<Stop> result = response.body();
                        ArrayList<ClusteredItem>mMarkerArray = new ArrayList<>();
                        mClusterManager = new ClusterManager(MapsActivity.this, mMap);
                        mClusterManager.setRenderer(new OwnIconRendered(MapsActivity.this.getApplicationContext(), mMap, mClusterManager));
                        for (Stop stop: result)
                        {
                            ClusteredItem item = new ClusteredItem(Double.valueOf(stop.getLatitude()),Double.valueOf(stop.getLongitude()), stop.getName());

                            mClusterManager.addItem(item);
                            mMarkerArray.add(item);
                        }
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        for (ClusteredItem marker : mMarkerArray) {
                            builder.include(marker.getPosition());
                        }
                        LatLngBounds bounds = builder.build();
                        int padding = 0; // offset from edges of the map in pixels
                        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, padding));
                    }

                    @Override
                    public void onFailure(Call<List<Stop>> call, Throwable t) {
                        t.printStackTrace();
                    }
                });
                //List<Stop> result = call.execute().body();
            } catch (Exception e) {
                e.printStackTrace();
            }
            //endregion
            //region Markers
            mMap.setOnMarkerClickListener(mClusterManager);
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener(){

                @Override
                public boolean onMarkerClick(Marker marker) {
                    LinearLayout mapContainer = (LinearLayout) findViewById(R.id.map);
                    int container_height = mapContainer.getHeight();

                    Projection projection = mMap.getProjection();

                    LatLng markerLatLng = new LatLng(marker.getPosition().latitude,
                            marker.getPosition().longitude);
                    Point markerScreenPosition = projection.toScreenLocation(markerLatLng);
                    Point pointHalfScreenAbove = new Point(markerScreenPosition.x,
                            markerScreenPosition.y);

                    LatLng aboveMarkerLatLng = projection.fromScreenLocation(pointHalfScreenAbove);
                    if(marker.getTitle() !=null)
                        marker.showInfoWindow();
                    CameraUpdate center = CameraUpdateFactory.newLatLng(aboveMarkerLatLng);
                    mMap.animateCamera(center);
                    return true;
                }
            });
            //endregion
            CameraPosition mPreviousCameraPosition = null;
            mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                @Override
                public void onCameraIdle() {
                    CameraPosition position = mMap.getCameraPosition();

                    mClusterManager.cluster();

                }
            });
            //endregion

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //region Getters methodes
    public void getStops(){
        JSONObject json;
        try {

            BufferedReader is = new BufferedReader(new InputStreamReader((getResources().openRawResource(R.raw.stops))));
            StringBuilder sb = new StringBuilder();
            String nextLine;
            while ((nextLine = is.readLine()) != null)
            {
                sb.append(nextLine);
            }

            json = new JSONObject(sb.toString());
            JSONArray ja = json.getJSONArray("stops");
            Gson myGson = new Gson();
            for(int i = 0; i < ja.length(); i++){
                JSONObject object = ja.getJSONObject(i);
                listStop.add(new Stop(
                    object.getInt("id"),
                    object.getString("name"),
                    object.getString("latitude"),
                    object.getString("longitude")
                ));
            }

        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }

    }
    //endregion
    //region ClickListener
    public void ClickCalc(View view) {
        Intent i = new Intent(getApplicationContext(), CalculateScheduleActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        getApplicationContext().startActivity(i);
    }
    public void ClickSchedule(View view) {
        TextView tv = (TextView) view;
        Intent i = new Intent(getApplicationContext(), StopListActivity.class);
        i.putExtra("line_id", tv.getText());
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        getApplicationContext().startActivity(i);
    }




    //endregion
    //region Drawer
    private void initDrawer() {
        mDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                mLanguette.setX(slideOffset * mDrawerList.getWidth());
            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

    public void OpenDrawerFromTip(View view) {
        mDrawerLayout.openDrawer(GravityCompat.START);
    }
    //endregion
}
