package com.alric.iem.appbourgtub.ui;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.alric.iem.appbourgtub.R;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        if (isConnected()) {
            System.out.println("test");
            Intent intent = new Intent(Splash.this, MapsActivity.class);
            startActivity(intent);
            finish();
        }
    }

    public boolean isConnected() {
        boolean isConnected = false;
        boolean isWiFi = false;

        ConnectivityManager cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);


        //On récupère les informations sur l'état de la connexion
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        try {
            isConnected = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
            assert activeNetwork != null;
            isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
        } catch (Exception e) {
            Log.e("Erreur :", String.valueOf(e));
        }

        //On affiche le résultat
        String string = "";
        if (isConnected) {
            if (isWiFi) {
                string = getString(R.string.connectedWifi);
            } else {
                string = getString(R.string.connected3G);
            }
            Toast.makeText(this, string, Toast.LENGTH_SHORT).show();
            return true;
        } else {
            string = getString(R.string.notConnected);
            Toast.makeText(this, string, Toast.LENGTH_LONG).show();
            return false;
        }
    }



}
