package com.alric.iem.appbourgtub.ui;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.alric.iem.appbourgtub.R;
import com.alric.iem.appbourgtub.api.model.Horaire;
import com.alric.iem.appbourgtub.api.model.Stop;
import com.alric.iem.appbourgtub.api.model.StopLine;
import com.alric.iem.appbourgtub.net.Api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StopListActivity extends AppCompatActivity {


    // Variables de classe
    List<Horaire> listeHoraire;
    ListView lvTimes;
    ListView lvNoms;
    //Synchronisation verticale des listView
    View clickSource;
    View touchSource;
    int offset = 0;
    ArrayAdapter<String> arrayadapter;
    List<Horaire> arrayHoraire = new ArrayList<Horaire>();
    List<StopLine> mListSL = new ArrayList<StopLine>();
    List<List<StopLine>> mListSLByStop = new ArrayList<List<StopLine>>();
    List<String> mListStr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_stops);

        //Récupération de la liste des horaires de la liste
        Intent i = getIntent();
        String line_id = i.getStringExtra("line_id");


        getStopLineList(line_id);

    }

    public void getStopLineList(String line_id){
        Api api = new Api();
        final Call<List<StopLine>> call = api.getScheduleByLine(line_id);

        call.enqueue(new Callback<List<StopLine>>() {
            @Override
            public void onResponse(Call<List<StopLine>> call, Response<List<StopLine>> response) {
                mListSL = response.body();
                if (mListSL.size() > 0) {
                    Collections.sort(mListSL, new Comparator<StopLine>() {
                        @Override
                        public int compare(final StopLine object1, final StopLine object2) {
                            return object1.getName().compareTo(object2.getName());
                        }
                    });
                }
                for (int i = 0 ; i < mListSL.size(); i++){
                    if(i == 0){
                        mListSLByStop.add(new ArrayList<StopLine>());
                        mListSLByStop.get(mListSLByStop.size() - 1).add(mListSL.get(i));
                    }
                    else
                    {
                        if(mListSL.get(i - 1).getName().equals(mListSL.get(i).getName())){
                            mListSLByStop.get(mListSLByStop.size() - 1).add(mListSL.get(i));
                        }
                        else{
                            mListSLByStop.add(new ArrayList<StopLine>());
                            mListSLByStop.get(mListSLByStop.size() - 1).add(mListSL.get(i));
                        }
                    }
                }
                for (int i = 0; i < mListSLByStop.size(); i++)
                {
                    List<String> mHoraireArray = new ArrayList<String>();
                    for(int j = 0; j < mListSLByStop.get(i).size(); j++){
                        if(mListSLByStop.get(i).get(j) == null ){
                            mHoraireArray.add("....");
                        }
                        else{
                            mHoraireArray.add(mListSLByStop.get(i).get(j).getTime());

                        }

                    }
                    arrayHoraire.add(new Horaire(mHoraireArray.toArray(new String[mHoraireArray.size()]), mListSLByStop.get(i).get(0).getName(), mListSLByStop.get(i).get(0).getOrder()));
                }
                initListViews();
            }

            @Override
            public void onFailure(Call<List<StopLine>> call, Throwable t) {
                Log.i("DEV2", t.toString());
                System.out.println("err");
            }
        });
    }

    public void initListViews(){
        lvTimes = (ListView) findViewById(R.id.listviewHoraires);
        lvNoms = (ListView) findViewById(R.id.listviewNoms);
        List<Horaire> liste = arrayHoraire;
        List<String> noms = new ArrayList<>();
        Collections.sort(arrayHoraire, new Comparator<Horaire>() {
            @Override
            public int compare(final Horaire object1, final Horaire object2) {
                return Integer.compare(object1.getOrder(), object2.getOrder());
            }
        });
        for(int j = 0; j < liste.size();j++){
            noms.add(liste.get(j).getNom());
        }

        ArrayAdapter<Horaire> adapter = new ArrayAdapter<Horaire>(this,android.R.layout.simple_list_item_1,liste) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View view = super.getView(position, convertView, parent);

                TextView textview = (TextView) view.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                textview.setTypeface(Typeface.MONOSPACE);
                return view;
            }
        };
        ArrayAdapter<String> adapterNom = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, noms) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                View view = super.getView(position, convertView, parent);

                TextView textview = (TextView) view.findViewById(android.R.id.text1);

                //Set your Font Size Here.
                return view;
            }
        };



        // Affichage des 2 Listviews
        lvTimes.setAdapter(adapter);
        lvNoms.setAdapter(adapterNom);
        makeListeners();
    }

    public void makeListeners(){
        //region Listeners Synchro
        // ----------- Listeners pour la synchro verticale ---------- //
        lvTimes.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(touchSource == null)
                    touchSource = v;

                if(v == touchSource) {
                    lvNoms.dispatchTouchEvent(event);
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        clickSource = v;
                        touchSource = null;
                    }
                }

                return false;
            }
        });
        lvTimes.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(view == clickSource)
                    lvNoms.setSelectionFromTop(firstVisibleItem, view.getChildAt(0).getTop() + offset);
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}
        });



        lvNoms.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(touchSource == null)
                    touchSource = v;

                if(v == touchSource) {
                    lvTimes.dispatchTouchEvent(event);
                    if(event.getAction() == MotionEvent.ACTION_UP) {
                        clickSource = v;
                        touchSource = null;
                    }
                }

                return false;
            }
        });
        lvNoms.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(view == clickSource)
                    lvTimes.setSelectionFromTop(firstVisibleItem, view.getChildAt(0).getTop() + offset);
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {}
        });
        //endregion
    }


}
